
-- Create tables
CREATE TABLE kh_Kontrahent (
    kh_Id INT NOT NULL PRIMARY KEY,
    kh_Symbol VARCHAR(255),
);

CREATE TABLE dkr_Dokument (
    dkr_Id INT NOT NULL PRIMARY KEY,
    dkr_IdKh INT FOREIGN KEY REFERENCES kh_Kontrahent(kh_Id),
	dkr_Kwota FLOAT,
	dkr_DokumentZrodlowy VARCHAR(255),
	dkr_DataDokumentu DATETIME
);

-- Insert test data
INSERT INTO dbo.kh_Kontrahent (kh_Id, kh_Symbol) VALUES (1, 'ABC');
INSERT INTO dbo.kh_Kontrahent (kh_Id, kh_Symbol) VALUES (2, 'DEF');

INSERT INTO dbo.dkr_Dokument (dkr_Id, dkr_IdKh, dkr_Kwota, dkr_DokumentZrodlowy, dkr_DataDokumentu) VALUES (1, 1, 10.10 ,'FS 1/2020','2020-04-02T00:00:00');
INSERT INTO dbo.dkr_Dokument (dkr_Id, dkr_IdKh, dkr_Kwota, dkr_DokumentZrodlowy, dkr_DataDokumentu) VALUES (2, 1, 20.00 ,'FS 2/2020','2020-04-02T00:00:00');
INSERT INTO dbo.dkr_Dokument (dkr_Id, dkr_IdKh, dkr_Kwota, dkr_DokumentZrodlowy, dkr_DataDokumentu) VALUES (3, 1, 30.00 ,'KG 1/2020','2020-04-02T00:00:00');
INSERT INTO dbo.dkr_Dokument (dkr_Id, dkr_IdKh, dkr_Kwota, dkr_DokumentZrodlowy, dkr_DataDokumentu) VALUES (4, 1, 1000.00 ,'FS 3/2020','2020-05-02T00:00:00');
INSERT INTO dbo.dkr_Dokument (dkr_Id, dkr_IdKh, dkr_Kwota, dkr_DokumentZrodlowy, dkr_DataDokumentu) VALUES (5, 1, 2000.00 ,'FS 4/2020','2020-03-02T00:00:00');

INSERT INTO dbo.dkr_Dokument (dkr_Id, dkr_IdKh, dkr_Kwota, dkr_DokumentZrodlowy, dkr_DataDokumentu) VALUES (6, 2, 100.10 ,'FS 8/2020','2020-04-02T00:00:00');
INSERT INTO dbo.dkr_Dokument (dkr_Id, dkr_IdKh, dkr_Kwota, dkr_DokumentZrodlowy, dkr_DataDokumentu) VALUES (7, 2, 209.00 ,'FS 9/2020','2020-04-02T00:00:00');
INSERT INTO dbo.dkr_Dokument (dkr_Id, dkr_IdKh, dkr_Kwota, dkr_DokumentZrodlowy, dkr_DataDokumentu) VALUES (8, 2, 300.00 ,'KG 1/2020','2020-04-02T00:00:00');
INSERT INTO dbo.dkr_Dokument (dkr_Id, dkr_IdKh, dkr_Kwota, dkr_DokumentZrodlowy, dkr_DataDokumentu) VALUES (9, 2, 1000.00 ,'FS 10/2020','2020-05-02T00:00:00');
INSERT INTO dbo.dkr_Dokument (dkr_Id, dkr_IdKh, dkr_Kwota, dkr_DokumentZrodlowy, dkr_DataDokumentu) VALUES (10, 2, 2000.00 ,'FS 12/2020','2020-03-02T00:00:00');


-- Select query
SELECT kh.kh_Symbol AS Klient, SUM(dkr.dkr_Kwota) AS 'Wartość Sprzedaży'
	FROM dbo.kh_Kontrahent kh left join dbo.dkr_Dokument dkr ON kh.kh_Id = dkr.dkr_IdKh
	WHERE dkr.dkr_DokumentZrodlowy like 'FS%'
		and DATEPART(m, dkr.dkr_DataDokumentu) = DATEPART(m, DATEADD(m, -1, getdate()))
		and DATEPART(yyyy, dkr.dkr_DataDokumentu) = DATEPART(yyyy, DATEADD(m, -1, getdate()))
	GROUP BY kh.kh_Symbol