﻿using FakeItEasy;
using Solex.API.Services;
using Solex.DataBase.Models;
using Xunit;

namespace Solex.API.Tests
{
    public class PriceCalculatorTests
    {
        [Fact]
        public void TestPriceCalculationForQuantityLessThan10()
        {
            var testProduct = new Product() { ProductId = 1, Quantity = 5 };
            var expectedResult = testProduct.Quantity * 5;
            var priceService = A.Fake<PriceCalculator>();

            var result = priceService.CalculateProductCost(testProduct);

            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void TestPriceCalculationForQuantityHigherThan10()
        {
            var testProduct = new Product() { ProductId = 1, Quantity = 20 };
            var expectedResult = testProduct.Quantity * 10;
            var priceService = A.Fake<PriceCalculator>();

            var result = priceService.CalculateProductCost(testProduct);

            Assert.Equal(expectedResult, result);
        }

    }
}
