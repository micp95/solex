﻿using Bogus;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Solex.API.Services;
using Solex.DataBase;
using Solex.DataBase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Solex.API.Tests
{
    public class CartServiceTests
    {

        [Fact]
        public async void CreateNewCartTest()
        {
            var cartId = 1;

            var fakeCarts = new List<Cart>();
            var fakePriceService = A.Fake<PriceCalculator>();
            var fakeCartContext = CreateFakeCartContextForCarts(fakeCarts);
            var fakeLogger = A.Fake<ILogger<CartService>>();

            var cartService = new CartService(fakeCartContext, fakePriceService, fakeLogger);
            var result = await cartService.GetProducts(cartId);

            //Check if saved to DB
            A.CallTo(() => fakeCartContext.Carts.AddAsync(A<Cart>.That.Matches(s => s.CartId == cartId), A<CancellationToken>._))
                .MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeCartContext.SaveChangesAsync(A<CancellationToken>._))
                .MustHaveHappenedOnceExactly();
            //Check if cart is empty
            Assert.False(result.Any());
        }

        [Fact]
        public async void CreateNewCartExceptionTest()
        {
            var cartId = 1;
            var exceptionMessage = "DB Exception";

            var fakeCarts = new List<Cart>();
            var fakePriceService = A.Fake<PriceCalculator>();
            var fakeCartContext = CreateFakeCartContextForCarts(fakeCarts);
            var fakeLogger = A.Fake<ILogger<CartService>>();

            //throw exceprion from SaveChangesAsync function
            A.CallTo(() => fakeCartContext.SaveChangesAsync(A<CancellationToken>._)).Throws(callObject => new Exception(exceptionMessage));

            Task result = Task.Delay(0);
            try
            {
                var cartService = new CartService(fakeCartContext, fakePriceService, fakeLogger);
                result = cartService.GetProducts(cartId);
                await result;
            }
            catch (Exception) { }


            //check exception in response
            Assert.NotNull(result.Exception);
            Assert.Contains(exceptionMessage, result.Exception.Message);
        }


        [Fact]
        public async void GetCartFromDBTest()
        {
            var cartId = 1;

            var fakeCarts = new List<Cart> { new Cart(cartId) };
            var fakePriceService = A.Fake<PriceCalculator>();
            var fakeCartContext = CreateFakeCartContextForCarts(fakeCarts);
            var fakeLogger = A.Fake<ILogger<CartService>>();

            var cartService = new CartService(fakeCartContext, fakePriceService, fakeLogger);
            var result = await cartService.GetProducts(cartId);

            //Check if not saved new cart to DB
            A.CallTo(() => fakeCartContext.Carts.AddAsync(A<Cart>.That.Matches(s => s.CartId == cartId), A<CancellationToken>._))
                .MustNotHaveHappened();

            //Check if cart is empty
            Assert.False(result.Any());
        }

        [Fact]
        public async void AddNewProductTest()
        {
            var cartId = 1;

            var fakeCarts = new List<Cart> { new Cart(cartId) };
            var fakeProduct = new Product() { ProductId = 1, Quantity = 5 };
            var fakePriceService = A.Fake<PriceCalculator>();
            var fakeCartContext = CreateFakeCartContextForCarts(fakeCarts);
            var fakeLogger = A.Fake<ILogger<CartService>>();

            var cartService = new CartService(fakeCartContext, fakePriceService, fakeLogger);
            var result = cartService.AddProduct(cartId, fakeProduct);
            await result;


            //Check error and product in cart
            Assert.Null(result.Exception);
            Assert.NotEmpty(fakeCarts[0].Products);
            Assert.Equal(fakeProduct,fakeCarts[0].Products.FirstOrDefault());
        }

        [Fact]
        public async void AddToExistProductTest()
        {
            var cartId = 1;
            var firstProductQuantity = 5;

            var fakeCarts = new List<Cart> { new Cart() { CartId = cartId, 
                                                          Products = new List<Product> {
                                                              new Product() { ProductId = 1, Quantity = firstProductQuantity } } } };
            var fakeProduct = new Product() { ProductId = 1, Quantity = 5 };
            var fakePriceService = A.Fake<PriceCalculator>();
            var fakeCartContext = CreateFakeCartContextForCarts(fakeCarts);
            var fakeLogger = A.Fake<ILogger<CartService>>();

            var cartService = new CartService(fakeCartContext, fakePriceService, fakeLogger);
            var result = cartService.AddProduct(cartId, fakeProduct);
            await result;

            //Check error and product in cart
            Assert.Null(result.Exception);
            Assert.Equal(firstProductQuantity + fakeProduct.Quantity, fakeCarts[0].Products.FirstOrDefault().Quantity);
        }


        [Fact]
        public async void DeleteProductFromCartTest()
        {
            var cartId = 1;
            var productId = 1;

            var fakeCarts = new List<Cart> { new Cart() { CartId = cartId,
                                                          Products = new List<Product> {
                                                              new Product() { ProductId = productId, Quantity = 1 } } } };

            var fakePriceService = A.Fake<PriceCalculator>();
            var fakeCartContext = CreateFakeCartContextForCarts(fakeCarts);
            var fakeLogger = A.Fake<ILogger<CartService>>();

            var cartService = new CartService(fakeCartContext, fakePriceService, fakeLogger);
            var result = cartService.DeleteProduct(cartId, productId);
            await result;

            //Check if removed from DB
            A.CallTo(() => fakeCartContext.Products.Remove(A<Product>._)).MustHaveHappenedOnceExactly();

            //Check error
            Assert.Null(result.Exception);
        }

        [Fact]
        public async void DeleteProductWithWrongIDFromCartTest()
        {
            var cartId = 1;
            var productId = 1;

            var fakeCarts = new List<Cart> { new Cart() { CartId = cartId,
                                                          Products = new List<Product> {
                                                              new Product() { ProductId = productId+1, Quantity = 1 } } } };

            var fakePriceService = A.Fake<PriceCalculator>();
            var fakeCartContext = CreateFakeCartContextForCarts(fakeCarts);
            var fakeLogger = A.Fake<ILogger<CartService>>();

            var cartService = new CartService(fakeCartContext, fakePriceService, fakeLogger);
            var result = cartService.DeleteProduct(cartId, productId);
            await result;

            //Check if not removed from DB
            A.CallTo(() => fakeCartContext.Products.Remove(A<Product>._)).MustNotHaveHappened();

            //Check error
            Assert.Null(result.Exception);
        }

        [Fact]
        public async void GetProductsTest()
        {
            var cartId = 1;

            var fakeProducts = GenerateProducts(20);
            var fakeCarts = new List<Cart> { new Cart() { CartId = cartId,
                                                          Products = fakeProducts } };
            var fakePriceService = A.Fake<PriceCalculator>();
            var fakeCartContext = CreateFakeCartContextForCarts(fakeCarts);
            var fakeLogger = A.Fake<ILogger<CartService>>();

            var cartService = new CartService(fakeCartContext, fakePriceService, fakeLogger);
            var result = cartService.GetProducts(cartId);
            await result;

            //Check error and check if products are equal
            Assert.Null(result.Exception);
            Assert.True(fakeProducts.SequenceEqual(result.Result));
        }

        [Fact]
        public async void GetCartCostTest()
        {
            var cartId = 1;

            var fakeProducts = GenerateProducts(20);
            var fakeCarts = new List<Cart> { new Cart() { CartId = cartId,
                                                          Products = fakeProducts } };
            var fakePriceService = A.Fake<IPriceCalculator>();
            var fakeCartContext = CreateFakeCartContextForCarts(fakeCarts);
            var fakeLogger = A.Fake<ILogger<CartService>>();

            //Change rules to calculate product price
            //Original rules have two unit tests: TestPriceCalculationForQuantityLessThan10 and TestPriceCalculationForQuantityHigherThan10
            A.CallTo(() => fakePriceService.CalculateProductCost(A<Product>._)).Returns(1);
            var expectedPrice = fakeProducts.Count;

            var cartService = new CartService(fakeCartContext, fakePriceService, fakeLogger);
            var result = cartService.GetCartCost(cartId);
            await result;

            //Check error and check if products are equal
            Assert.Null(result.Exception);
            Assert.Equal(expectedPrice, result.Result);
        }

        private ICollection<Product> GenerateProducts(int numberOfProducts)
        {
            var productIdCounter = 0;
            var minFloatNumber = 0;
            var maxFloatNumber = 100;
            Randomizer.Seed = new Random();

            var testOrders = new Faker<Product>()
                //Ensure all properties have rules. By default, StrictMode is false
                //Set a global policy by using Faker.DefaultStrictMode
                .StrictMode(true)
                .RuleFor(o => o.ProductId, f => productIdCounter++)
                .RuleFor(o => o.Quantity, f => f.Random.Float()* maxFloatNumber + minFloatNumber);
            
            return testOrders.Generate(numberOfProducts);
        }

        private CartContext CreateFakeCartContextForCarts(IEnumerable<Cart> cartsToAdd)
        {
            IQueryable<Cart> fakeIQueryable = cartsToAdd.AsQueryable();

            //Create fakes
            var fakeCartContext = A.Fake<CartContext>();
            var fakeCartDbSet = A.Fake<DbSet<Cart>>(d => d.Implements(typeof(IQueryable<Cart>)));
            var fakeProductDbSet = A.Fake<DbSet<Product>>(d => d.Implements(typeof(IQueryable<Product>)));

            //Mocks for carts collection
            A.CallTo(() => ((IQueryable<Cart>)fakeCartDbSet).Provider).Returns(fakeIQueryable.Provider);
            A.CallTo(() => ((IQueryable<Cart>)fakeCartDbSet).Expression).Returns(fakeIQueryable.Expression);

            //Mocks for fakeCartContext methods and fields
            A.CallTo(() => fakeCartContext.Carts).Returns(fakeCartDbSet);
            A.CallTo(() => fakeCartContext.Carts.AddAsync(A<Cart>._, A<CancellationToken>._)).Returns(new ValueTask<EntityEntry<Cart>>());
            A.CallTo(() => fakeCartContext.Products).Returns(fakeProductDbSet);
            A.CallTo(() => fakeCartContext.Products.Remove(A<Product>._)).Returns((EntityEntry<Product>)null);
            A.CallTo(() => fakeCartContext.SaveChangesAsync(A<CancellationToken>._)).Returns(Task.FromResult(1));

            return fakeCartContext;
        }

    }



}
