﻿using FakeItEasy;
using Microsoft.Extensions.Logging;
using Solex.API.Controllers;
using Solex.API.Models;
using Solex.API.Services;
using Solex.DataBase.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Solex.API.Tests
{
    public class CartControllerTests
    {
        [Fact]
        public async void ExecuteSuccessfulRequestTest()
        {
            var cartId = 1;

            var fakeProduct = A.Fake<Product>();
            var fakeLogger = A.Fake<ILogger<CartController>>();
            var fakeCartService = A.Fake<ICartService>();

            A.CallTo(() => fakeCartService.AddProduct(A<int>._, A<Product>._)).Returns(Task.Delay(0));

            var cartController = new CartController(fakeLogger, fakeCartService);
            var result = cartController.AddToCart(cartId, fakeProduct);
            await result;

            //check response
            Assert.Null(result.Exception);
            Assert.Equal(200,result.Result.StatusCode);
            Assert.Null(result.Result.ErrorMessage);
        }

        [Fact]
        public async void ExecuteFailedRequestTest()
        {
            var cartId = 1;
            var exceptionMessage = "Add Product Exception";

            var fakeProduct = A.Fake<Product>();
            var fakeLogger = A.Fake<ILogger<CartController>>();
            var fakeCartService = A.Fake<ICartService>();

            A.CallTo(() => fakeCartService.AddProduct(A<int>._, A<Product>._)).Throws(new Exception(exceptionMessage));

            Task<ResponseBase> result = Task.FromResult((ResponseBase)null);
  
            var cartController = new CartController(fakeLogger, fakeCartService);
            result = cartController.AddToCart(cartId, fakeProduct);
            await result;

            //check response
            Assert.Null(result.Exception);
            Assert.Equal(500, result.Result.StatusCode);
            Assert.NotNull(result.Result.ErrorMessage);
            Assert.Contains(exceptionMessage, result.Result.ErrorMessage);
        }

        [Fact]
        public async void AddCartTest()
        {
            var cartId = 1;

            var fakeProduct = A.Fake<Product>();
            var fakeLogger = A.Fake<ILogger<CartController>>();
            var fakeCartService = A.Fake<ICartService>();

            A.CallTo(() => fakeCartService.AddProduct(A<int>.That.Matches(x=>x == cartId), A<Product>.That.Matches(x=>x.Equals(fakeProduct)))).Returns(Task.Delay(0));

            var cartController = new CartController(fakeLogger, fakeCartService);
            var result = cartController.AddToCart(cartId, fakeProduct);
            await result;

            //check response
            Assert.Null(result.Exception);
            Assert.Equal(200, result.Result.StatusCode);
            Assert.Null(result.Result.ErrorMessage);
        }

        [Fact]
        public async void DeleteFromCartTest()
        {
            var cartId = 1;
            var productId = 1;

            var fakeLogger = A.Fake<ILogger<CartController>>();
            var fakeCartService = A.Fake<ICartService>();

            A.CallTo(() => fakeCartService.DeleteProduct(A<int>.That.Matches(x => x == cartId), A<int>.That.Matches(x => x == productId))).Returns(Task.Delay(0));

            var cartController = new CartController(fakeLogger, fakeCartService);
            var result = cartController.DeleteFromCart(cartId, productId);
            await result;

            //check response
            Assert.Null(result.Exception);
            Assert.Equal(200, result.Result.StatusCode);
            Assert.Null(result.Result.ErrorMessage);
        }

        [Fact]
        public async void GetProductsTest()
        {
            var cartId = 1;

            var fakeLogger = A.Fake<ILogger<CartController>>();
            var fakeCartService = A.Fake<ICartService>();
            var fakeServiceResponse = A.Fake<IEnumerable<Product>>();

            A.CallTo(() => fakeCartService.GetProducts(A<int>.That.Matches(x => x == cartId))).Returns(Task.FromResult(fakeServiceResponse));

            var cartController = new CartController(fakeLogger, fakeCartService);
            var result = cartController.GetProducts(cartId);
            await result;

            //check response
            Assert.Null(result.Exception);
            Assert.Equal(200, result.Result.StatusCode);
            Assert.Null(result.Result.ErrorMessage);
            Assert.Equal(fakeServiceResponse, result.Result.ResponseObject);
        }

        [Fact]
        public async void GetCartCostTest()
        {
            var cartId = 1;

            var fakeLogger = A.Fake<ILogger<CartController>>();
            var fakeCartService = A.Fake<ICartService>();
            var serviceResponse = 20f;

            A.CallTo(() => fakeCartService.GetCartCost(A<int>.That.Matches(x => x == cartId))).Returns(Task.FromResult(serviceResponse));

            var cartController = new CartController(fakeLogger, fakeCartService);
            var result = cartController.GetCartCost(cartId);
            await result;

            //check response
            Assert.Null(result.Exception);
            Assert.Equal(200, result.Result.StatusCode);
            Assert.Null(result.Result.ErrorMessage);
            Assert.Equal(serviceResponse, result.Result.ResponseObject);
        }
    }
}
