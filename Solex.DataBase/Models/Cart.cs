﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Solex.DataBase.Models
{
    public class Cart
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.None)]
        public int CartId { get; set; }
        public ICollection<Product> Products { get; set; }

        public Cart(int cartId)
        {
            CartId = cartId;
            Products = new List<Product>();
        }
        public Cart() {
            Products = new List<Product>();
        }
    }

}
