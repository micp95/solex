﻿using Microsoft.EntityFrameworkCore;
using Solex.DataBase.Models;

namespace Solex.DataBase
{
    public class CartContext : DbContext
    {
        public virtual DbSet<Cart> Carts { get; set; }
        public virtual DbSet<Product> Products { get; set; }

        public CartContext(DbContextOptions<CartContext> options): base(options) { }
        public CartContext(){ }
    }
}
