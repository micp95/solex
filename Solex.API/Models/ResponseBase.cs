﻿namespace Solex.API.Models
{
    public class ResponseBase
    {
        public int StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public object ResponseObject { get; set; }
    }
}
