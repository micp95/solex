﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Solex.API.Models;
using Solex.API.Services;
using Solex.DataBase.Models;
using System;
using System.Threading.Tasks;

namespace Solex.API.Controllers
{
    [ApiController]
    [Route("Koszyk")]
    public class CartController : ControllerBase
    {
        private readonly ILogger<CartController> _logger;
        private readonly ICartService _cartService;

        public CartController(ILogger<CartController> logger, ICartService cartService)
        {
            _logger = logger;
            _cartService = cartService;
        }

        [HttpPost]
        [Route("DodajDoKoszyka")]
        public async Task<ResponseBase> AddToCart(int cartId, Product product)
        {
            return await ExecuteMethod(async () =>
            {
                await _cartService.AddProduct(cartId, product);
                return new ResponseBase();
            });
        }

        [HttpDelete]
        [Route("UsunZKoszyka")]
        public async Task<ResponseBase> DeleteFromCart(int cartId, int productId)
        {
            return await ExecuteMethod(async () =>
            {
                await _cartService.DeleteProduct(cartId, productId);
                return new ResponseBase();
            });
        }

        [HttpGet]
        [Route("PobierzKoszyk")]
        public async Task<ResponseBase> GetProducts(int cartId)
        {
            return await ExecuteMethod(async () =>
            {
                var res = await _cartService.GetProducts(cartId);
                return new ResponseBase() { ResponseObject = res };
            });
        }

        [HttpGet]
        [Route("PobierzKoszykWartosc")]
        public async Task<ResponseBase> GetCartCost(int cartId)
        {
            return await ExecuteMethod(async () =>
            {
                var res = await _cartService.GetCartCost(cartId);
                return new ResponseBase() { ResponseObject = res };
            });
        }

        // Wrapper for execute controller methods
        private async Task<ResponseBase> ExecuteMethod(Func<Task<ResponseBase>> function)
        {
            ResponseBase result = new ResponseBase();
            try
            {
                result = await function();
                result.StatusCode = 200;
            }
            catch(Exception ex){
                result.ErrorMessage = ex.Message;
                result.StatusCode = 500;
                _logger.LogError(ex,"Request failed!");
            }
            return result;
        }
    }
}
