﻿using Solex.DataBase.Models;

namespace Solex.API.Services
{
    public class PriceCalculator : IPriceCalculator
    {
        public PriceCalculator() { }

        public float CalculateProductCost(Product product)
        {
            if(product.Quantity > 10)
            {
                return product.Quantity * 10;
            }
            else
            {
                return product.Quantity * 5;
            }
        }
    }
}
