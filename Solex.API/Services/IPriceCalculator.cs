﻿using Solex.DataBase.Models;

namespace Solex.API.Services
{
    public interface IPriceCalculator
    {
        float CalculateProductCost(Product product);
    }
}
