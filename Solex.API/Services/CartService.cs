﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Solex.DataBase;
using Solex.DataBase.Models;

namespace Solex.API.Services
{
    public class CartService : ICartService
    {
        private readonly CartContext _cartContext;
        private readonly IPriceCalculator _priceCalculator;
        private readonly ILogger<CartService> _logger;
        public CartService(CartContext cartContext, IPriceCalculator priceCalculator, ILogger<CartService> logger)
        {
            _cartContext = cartContext;
            _priceCalculator = priceCalculator;
            _logger = logger;
        }

        public async Task AddProduct(int cartId, Product product)
        {
            _logger.LogInformation($"Add new product (id={product.ProductId}, Quantity={product.Quantity} ) to card {cartId}");
            var cart = await GetCart(cartId);
            
            var oldProduct = cart.Products.FirstOrDefault(x => x.ProductId == product.ProductId);
            if(oldProduct == null)
            {
                cart.Products.Add(product);
            }
            else
            {
                oldProduct.Quantity += product.Quantity;
            }

            await SubmitChanges();
        }

        public async Task DeleteProduct(int cartId, int productId)
        {
            _logger.LogInformation($"Delete product {productId} from cart {cartId}");
            var cart = await GetCart(cartId);

            var productToRemove = cart.Products.FirstOrDefault(x => x.ProductId == productId);
            if(productToRemove != null)
            {
                _cartContext.Products.Remove(productToRemove);
                await SubmitChanges();
            }
        }

        public async Task<IEnumerable<Product>> GetProducts(int cartId)
        {
            _logger.LogInformation($"Get products for cart {cartId}");
            var cart = await GetCart(cartId);

            return cart.Products;
        }
        public async Task<float> GetCartCost (int cartId)
        {
            _logger.LogInformation($"Get products cost for cart {cartId}");
            var products = await GetProducts(cartId);

            return products.Sum(product => _priceCalculator.CalculateProductCost(product));
        }



        private async Task<Cart> GetCart(int cartId)
        {
            Cart cart = null;
            try {
                cart = _cartContext.Carts.Where(x => x.CartId == cartId)
                                .Include(x => x.Products)
                                .FirstOrDefault();
                if (cart == null)
                    cart = await CreateCart(cartId);
            }
            catch(Exception ex)
            {
                throw new Exception($"Problem with capture cart (id: {cartId}): {ex.Message}", ex);
            }

            return cart;
        }

        private async Task<Cart> CreateCart(int cartId)
        {
            Cart cart;
            try
            {
                cart = new Cart(cartId);
                var k = await _cartContext.Carts.AddAsync(cart);
                await SubmitChanges();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Creating card {cartId} has been failed");
                throw new Exception($"Problem with create new cart (id: {cartId}): {ex.Message}", ex);
            }

            _logger.LogWarning($"New cart {cartId} has been created!");
            return cart;
        }

        private async Task SubmitChanges()
        {
            try
            {
                await _cartContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Save DB changes error");
                throw new Exception($"SQL - save changes error: {ex.Message}", ex);
            }
        }
    }
}
