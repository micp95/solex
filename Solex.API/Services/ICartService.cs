﻿using Solex.DataBase.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Solex.API.Services
{
    public interface ICartService
    {
        Task AddProduct(int cartId, Product product);
        Task DeleteProduct(int cartId, int productId);
        Task<IEnumerable<Product>> GetProducts(int cartId);
        Task<float> GetCartCost(int cartId);
    }
}
